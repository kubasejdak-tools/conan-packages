#include <iio.h>

#include <cstdio>
#include <cstdlib>

int main()
{
	unsigned int major{};
    unsigned int minor{};
	char tag[8];
    iio_library_get_version(&major, &minor, tag);
    std::printf("Library version: %u.%u (git tag: %s)\n", major, minor, tag);

    std::printf("Compiled with backends:");
    for (unsigned int i = 0; i < iio_get_backends_count(); ++i)
        std::printf(" %s", iio_get_backend(i));

	std::printf("\n");
    return EXIT_SUCCESS;
}
