from conans import ConanFile, CMake, tools
import os

class LibiioConan(ConanFile):
    name = "libiio"
    license = "LGPL-2.1-only"
    author = "Kuba Sejdak kuba.sejdak@gmail.com"
    url = "https://gitlab.com/embeddedlinux/tools/conan-packages"
    homepage = "http://analogdevicesinc.github.io/libiio/"
    description = "A cross-platform user library to access Industrial Input Output (IIO) devices"
    topics = ("iio", "libiio", "analog", "ad")
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    exports_sources = ["CMakeLists.txt", "patches/*"]
    options = {
        "network_backend": [True, False],
        "xml_backend": [True, False],
        "usb_backend": [True, False]
    }
    default_options = {
        "network_backend": False,
        "xml_backend": False,
        "usb_backend": False
    }

    _cmake = None
    _source_subfolder = "source_subfolder"

    def source(self):
        git = tools.Git(folder=self._source_subfolder)
        git.clone("https://github.com/analogdevicesinc/libiio.git", branch="v" + self.version, shallow=True)

        if self.version in self.conan_data["patches"]:
            for patch in self.conan_data["patches"][self.version]:
                tools.patch(**patch, base_path=self._source_subfolder)

    def requirements(self):
        if self.options.network_backend or self.options.xml_backend or self.options.usb_backend:
            self.requires("libxml2/2.9.10")

    def _configure_cmake(self):
        if self._cmake:
            return self._cmake

        self._cmake = CMake(self)
        self._cmake.definitions["WITH_NETWORK_BACKEND"] = "ON" if self.options.network_backend else "OFF"
        self._cmake.definitions["WITH_XML_BACKEND"] = "ON" if self.options.xml_backend else "OFF"
        self._cmake.definitions["WITH_USB_BACKEND"] = "ON" if self.options.usb_backend else "OFF"
        self._cmake.definitions["WITH_SERIAL_BACKEND"] = "OFF"

        self._cmake.configure()
        return self._cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()

        self.copy("COPYING.txt", dst="licenses")
        self.copy("*.h", dst="include", src="libiio")
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["iio"]

    def package_id(self):
        del self.info.settings.build_type
