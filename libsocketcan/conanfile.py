from conans import ConanFile, CMake, tools
import os

class LibsocketcanConan(ConanFile):
    name = "libsocketcan"
    license = "LGPL-2.1-only"
    author = "Kuba Sejdak kuba.sejdak@gmail.com"
    url = "https://gitlab.com/embeddedlinux/tools/conan-packages"
    homepage = "http://www.pengutronix.de"
    description = "Library that allows controling some basic functions in SocketCAN from userspace"
    topics = ("libsocketcan", "socketcan", "can")
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake", "cmake_find_package"
    exports_sources = ["CMakeLists.txt", "patches/*"]
    options = {
        "shared": [True, False]
    }
    default_options = {
        "shared": True
    }

    _cmake = None
    _source_subfolder = "source_subfolder"

    def source(self):
        tools.get(**self.conan_data["sources"][self.version])
        os.rename("libsocketcan-" + self.version, self._source_subfolder)

        if self.version in self.conan_data["patches"]:
            for patch in self.conan_data["patches"][self.version]:
                tools.patch(**patch, base_path=self._source_subfolder)

    def _configure_cmake(self):
        if self._cmake:
            return self._cmake

        self._cmake = CMake(self)
        self._cmake.definitions["BUILD_SHARED_LIBS"] = "ON" if self.options.shared else "OFF"

        self._cmake.configure()
        return self._cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["socketcan"]

    def package_id(self):
        del self.info.settings.build_type
