from conans import ConanFile, CMake, tools, errors
import os

class GrpcPluginsConan(ConanFile):
    name = "grpc-plugins"
    license = "Apache-2.0"
    author = "Kuba Sejdak kuba.sejdak@gmail.com"
    url = "https://gitlab.com/embeddedlinux/tools/conan-packages"
    homepage = "https://grpc.io/"
    description = "Protoc plugins for the C based gRPC (C++, Python, Ruby, Objective-C, PHP, C#)"
    topics = ("grpc", "rpc", "google", "plugins")
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake", "cmake_find_package"
    exports_sources = ["CMakeLists.txt", "patches/*"]
    options = {
        "shared": [True, False],
        "fPIC": [True, False],
        "build_csharp_ext": [True, False],
    }
    default_options = {
        "shared": False,
        "fPIC": True,
        "build_csharp_ext": False,
    }
    keep_imports = True

    _cmake = None
    _source_subfolder = "source_subfolder"

    def source(self):
        git = tools.Git(folder=self._source_subfolder)
        git.clone("https://github.com/grpc/grpc.git", branch="v" + self.version, shallow=True)

        if self.version in self.conan_data["patches"]:
            for patch in self.conan_data["patches"][self.version]:
                tools.patch(**patch, base_path=self._source_subfolder)

    def requirements(self):
        if self.version in self.conan_data["dependencies"]:
            dependencies = self.conan_data["dependencies"][self.version][0]
            for lib, version in dependencies.items():
                self.requires(lib + "/" + version)

    def imports(self):
        # grpc-plugins package provides also protoc for x86 to simplify usage in user's CMake.
        self.copy("protoc*", src="bin", dst="bin")
        self.copy("*.proto", src="include", dst="include")

    def _configure_cmake(self):
        if tools.cross_building(self.settings):
            raise errors.ConanException(self.name, "are only for the host system, cross-compiletion is not supported")

        if self._cmake:
            return self._cmake

        self._cmake = CMake(self)
        self._cmake.definitions["gRPC_ZLIB_PROVIDER"] = "package"
        self._cmake.definitions["gRPC_CARES_PROVIDER"] = "package"
        self._cmake.definitions["gRPC_SSL_PROVIDER"] = "package"
        self._cmake.definitions["gRPC_PROTOBUF_PROVIDER"] = "package"
        self._cmake.definitions["gRPC_BENCHMARK_PROVIDER"] = "none"
        self._cmake.definitions["gRPC_GFLAGS_PROVIDER"] = "none"
        self._cmake.definitions["gRPC_BUILD_CODEGEN"] = "ON"
        self._cmake.definitions["gRPC_BUILD_CSHARP_EXT"] = "ON" if self.options.build_csharp_ext else "OFF"
        self._cmake.definitions["gRPC_BUILD_TESTS"] = "OFF"
        self._cmake.configure()
        return self._cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build(target="plugins")

    def package(self):
        cmake = self._configure_cmake()

        self.copy(pattern="LICENSE", dst="licenses")
        self.copy("*", dst="include", src="include")
        self.copy("*.cmake", dst="lib", src="lib", keep_path=True)
        self.copy("*.lib", dst="lib", src="", keep_path=False)
        self.copy("*.a", dst="lib", src="", keep_path=False)
        self.copy("*", dst="bin", src="bin")
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)

    def package_info(self):
        bindir = os.path.join(self.package_folder, "bin")
        self.output.info("Appending PATH environment variable: {}".format(bindir))
        self.env_info.PATH.append(bindir)

    def package_id(self):
        del self.info.settings.build_type
