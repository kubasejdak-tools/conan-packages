#include <gpiod.h>

#include <cstdio>
#include <cstdlib>

int main()
{
    auto* it = gpiod_chip_iter_new();
    if (it == nullptr)
        return EXIT_SUCCESS;

    gpiod_chip *chip{};
    gpiod_foreach_chip(it, chip)
        std::printf("%s [%s] (%u lines)\n", gpiod_chip_name(chip), gpiod_chip_label(chip), gpiod_chip_num_lines(chip));

    gpiod_chip_iter_free(it);
    return EXIT_SUCCESS;
}
