from conans import ConanFile, CMake, tools

class GrpcTestConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake_find_package", "cmake_paths"

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def imports(self):
        self.copy("*", dst="bin", src="bin")
        self.copy("*", dst="bin", src='lib')

    def test(self):
        if not tools.cross_building(self.settings):
            self.run("./grpc-test")
