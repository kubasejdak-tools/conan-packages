#include <grpcpp/grpcpp.h>
#include <grpcpp/health_check_service_interface.h>

#include <cstdio>
#include <cstdlib>

int main()
{
    grpc::EnableDefaultHealthCheckService(true);

    return EXIT_SUCCESS;
}
