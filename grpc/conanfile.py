from conans import ConanFile, CMake, tools
import os

class GrpcConan(ConanFile):
    name = "grpc"
    license = "Apache-2.0"
    author = "Kuba Sejdak kuba.sejdak@gmail.com"
    url = "https://gitlab.com/embeddedlinux/tools/conan-packages"
    homepage = "https://grpc.io/"
    description = "The C based gRPC (C++, Python, Ruby, Objective-C, PHP, C#)"
    topics = ("grpc", "rpc", "google")
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake", "cmake_find_package"
    exports_sources = ["CMakeLists.txt", "patches/*"]
    options = {
        "shared": [True, False],
        "fPIC": [True, False],
        "build_codegen": [True, False],
        "build_csharp_ext": [True, False],
        "build_tests": [True, False]
    }
    default_options = {
        "shared": False,
        "fPIC": True,
        "build_codegen": True,
        "build_csharp_ext": False,
        "build_tests": False
    }

    _cmake = None
    _source_subfolder = "source_subfolder"

    def source(self):
        git = tools.Git(folder=self._source_subfolder)
        git.clone("https://github.com/grpc/grpc.git", branch="v" + self.version, shallow=True)

        if self.version in self.conan_data["patches"]:
            for patch in self.conan_data["patches"][self.version]:
                tools.patch(**patch, base_path=self._source_subfolder)

    def requirements(self):
        if self.version in self.conan_data["dependencies"]:
            dependencies = self.conan_data["dependencies"][self.version][0]
            for lib, version in dependencies.items():
                self.requires(lib + "/" + version)

    def build_requirements(self):
        dependencies = self.conan_data["dependencies"][self.version][0]
        self.build_requires("protobuf/" + dependencies["protobuf"])

        if tools.cross_building(self.settings):
            self.build_requires("grpc-plugins/{}@{}/{}".format(self.version, self.user, self.channel))

        if self.options.build_tests:
            self.build_requires("benchmark/1.5.0")
            self.build_requires("gflags/2.2.2")

    def _configure_cmake(self):
        if self._cmake:
            return self._cmake

        self._cmake = CMake(self)
        if tools.cross_building(self.settings):
            self._cmake.definitions["CMAKE_SYSTEM_NAME"] = self.settings.os
            build_protoc_path = list(set(self.deps_env_info["protobuf"].PATH))[0] + "/protoc"
            self.output.info("Using protoc in the build context: {}".format(build_protoc_path))
            self._cmake.definitions["BUILD_PROTOC_PATH"] = build_protoc_path
        self._cmake.definitions["gRPC_INSTALL"] = "ON"
        self._cmake.definitions["gRPC_ZLIB_PROVIDER"] = "package"
        self._cmake.definitions["gRPC_CARES_PROVIDER"] = "package"
        self._cmake.definitions["gRPC_SSL_PROVIDER"] = "package"
        self._cmake.definitions["gRPC_PROTOBUF_PROVIDER"] = "package"
        self._cmake.definitions["gRPC_BENCHMARK_PROVIDER"] = "package" if self.options.build_tests else "none"
        self._cmake.definitions["gRPC_GFLAGS_PROVIDER"] = "package" if self.options.build_tests else "none"
        self._cmake.definitions["gRPC_BUILD_CODEGEN"] = "ON" if self.options.build_codegen else "OFF"
        self._cmake.definitions["gRPC_BUILD_CSHARP_EXT"] = "ON" if self.options.build_csharp_ext else "OFF"
        self._cmake.definitions["gRPC_BUILD_TESTS"] = "ON" if self.options.build_tests else "OFF"
        self._cmake.configure()
        return self._cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()

        self.copy(pattern="LICENSE", dst="licenses")
        self.copy("*", dst="include", src="include")
        self.copy("*.cmake", dst="lib", src="lib", keep_path=True)
        self.copy("*.lib", dst="lib", src="", keep_path=False)
        self.copy("*.a", dst="lib", src="", keep_path=False)
        self.copy("*", dst="bin", src="bin")
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = [
            "grpc++_unsecure",
            "grpc++_reflection",
            "grpc++_error_details",
            "grpc++",
            "grpc_unsecure",
            "grpc_plugin_support",
            "grpc_cronet",
            "grpcpp_channelz",
            "grpc",
            "gpr",
            "address_sorting"
        ]

    def package_id(self):
        del self.info.settings.build_type
